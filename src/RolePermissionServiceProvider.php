<?php

namespace WeSimplyCode\RolePermission;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;

class RolePermissionServiceProvider extends ServiceProvider{

    public function boot(Filesystem $filesystem)
    {
        $this->publishes([
            __DIR__.'/../config/rolePermission.php' => config_path('rolePermission.php'),
        ], 'config');

        $this->publishes([
            __DIR__.'/../migrations/create_all_tables.php' => $this->getMigrationFileName($filesystem),
        ], 'migrations');
    }

    public function register()
    {

    }

    /**
     * Returns existing migration file if found, else uses the current timestamp.
     *
     * @param Filesystem $filesystem
     * @return string
     */
    protected function getMigrationFileName(Filesystem $filesystem): string
    {
        $timestamp = date('Y_m_d_His');
        return Collection::make($this->app->databasePath().DIRECTORY_SEPARATOR.'migrations'.DIRECTORY_SEPARATOR)
            ->flatMap(function ($path) use ($filesystem) {
                return $filesystem->glob($path.'*_create_permission_tables.php');
            })->push($this->app->databasePath()."/migrations/{$timestamp}_create_all_tables.php")
            ->first();
    }
}